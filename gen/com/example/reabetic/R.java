/* AUTO-GENERATED FILE.  DO NOT MODIFY.
 *
 * This class was automatically generated by the
 * aapt tool from the resource data it found.  It
 * should not be modified by hand.
 */

package com.example.reabetic;

public final class R {
    public static final class attr {
        /** <p>Must be a reference to another resource, in the form "<code>@[+][<i>package</i>:]<i>type</i>:<i>name</i></code>"
or to a theme attribute in the form "<code>?[<i>package</i>:][<i>type</i>:]<i>name</i></code>".
         */
        public static final int buttonBarButtonStyle=0x7f010001;
        /** <p>Must be a reference to another resource, in the form "<code>@[+][<i>package</i>:]<i>type</i>:<i>name</i></code>"
or to a theme attribute in the form "<code>?[<i>package</i>:][<i>type</i>:]<i>name</i></code>".
         */
        public static final int buttonBarStyle=0x7f010000;
    }
    public static final class color {
        public static final int black_overlay=0x7f050000;
    }
    public static final class dimen {
        /**  Default screen margins, per the Android Design guidelines. 

         Customize dimensions originally defined in res/values/dimens.xml (such as
         screen margins) for sw720dp devices (e.g. 10" tablets) in landscape here.
    
         */
        public static final int activity_horizontal_margin=0x7f060000;
        public static final int activity_vertical_margin=0x7f060001;
    }
    public static final class drawable {
        public static final int bakgrund=0x7f020000;
        public static final int bloddroppe=0x7f020001;
        public static final int blodsocker=0x7f020002;
        public static final int droppe=0x7f020003;
        public static final int ic_action_name=0x7f020004;
        public static final int ic_launcher=0x7f020005;
        public static final int ic_stat_name=0x7f020006;
        public static final int insulinspruta=0x7f020007;
        public static final int laggtill=0x7f020008;
        public static final int launchicon=0x7f020009;
        public static final int listview_gradient=0x7f02000a;
        public static final int litendroppe=0x7f02000b;
        public static final int litenspruta=0x7f02000c;
        public static final int litetbrev=0x7f02000d;
        public static final int login=0x7f02000e;
        public static final int message=0x7f02000f;
        public static final int newmessage=0x7f020010;
        public static final int newuser=0x7f020011;
        public static final int notis=0x7f020012;
        public static final int profil=0x7f020013;
        public static final int profil2=0x7f020014;
        public static final int profilpic=0x7f020015;
        public static final int profilpicstor=0x7f020016;
        public static final int reabeticlogotype=0x7f020017;
        public static final int sendmessage=0x7f020018;
        public static final int skapanyanvandare=0x7f020019;
        public static final int skickbrev=0x7f02001a;
        public static final int sprutany=0x7f02001b;
        public static final int startanimation=0x7f02001c;
        public static final int startsida=0x7f02001d;
    }
    public static final class id {
        public static final int Bluetoothfield=0x7f0a0004;
        public static final int InsulinTxtView=0x7f0a0022;
        public static final int ListBT=0x7f0a001a;
        public static final int MessageTxtView=0x7f0a0023;
        public static final int Pirimary=0x7f0a0020;
        public static final int Usertextfield=0x7f0a0021;
        public static final int action_settings=0x7f0a0024;
        public static final int choosePassword=0x7f0a0017;
        public static final int chooseUsername=0x7f0a0018;
        public static final int confirmbtn=0x7f0a0019;
        public static final int glucosefield=0x7f0a0012;
        public static final int imageView1=0x7f0a0010;
        public static final int imageView3=0x7f0a0014;
        public static final int insulinfield=0x7f0a0011;
        public static final int insulinlogo=0x7f0a0002;
        public static final int linearLayout1=0x7f0a0001;
        public static final int list2=0x7f0a000e;
        public static final int loginbtn2=0x7f0a000d;
        public static final int logo=0x7f0a000b;
        public static final int logotype=0x7f0a0016;
        public static final int messagebtn2=0x7f0a0007;
        public static final int messagefield=0x7f0a0013;
        public static final int messagefield2=0x7f0a0008;
        public static final int newMessageBtn=0x7f0a000f;
        public static final int newuserbtn=0x7f0a000c;
        public static final int notificationsbtn=0x7f0a0005;
        public static final int password=0x7f0a0009;
        public static final int profilbtn=0x7f0a0006;
        public static final int scrollView1=0x7f0a0000;
        public static final int sendMessage=0x7f0a0015;
        public static final int textView2=0x7f0a001f;
        public static final int textView3=0x7f0a001b;
        public static final int textView4=0x7f0a001e;
        public static final int textView5=0x7f0a001d;
        public static final int textView6=0x7f0a001c;
        public static final int theusername=0x7f0a0003;
        public static final int username=0x7f0a000a;
    }
    public static final class layout {
        public static final int activity_logged_in=0x7f030000;
        public static final int activity_login=0x7f030001;
        public static final int activity_message=0x7f030002;
        public static final int activity_new_message=0x7f030003;
        public static final int activity_new_profil=0x7f030004;
        public static final int activity_notifications=0x7f030005;
        public static final int activity_profil=0x7f030006;
        public static final int ist_v2=0x7f030007;
        public static final int ist_v3=0x7f030008;
        public static final int list_v=0x7f030009;
    }
    public static final class menu {
        public static final int animation=0x7f090000;
        public static final int database_stream=0x7f090001;
        public static final int logged_in=0x7f090002;
        public static final int login=0x7f090003;
        public static final int message=0x7f090004;
        public static final int new_message=0x7f090005;
        public static final int new_profil=0x7f090006;
        public static final int notifications=0x7f090007;
        public static final int profil=0x7f090008;
        public static final int xml_parsing=0x7f090009;
    }
    public static final class string {
        public static final int ChoosePassword=0x7f070010;
        public static final int ChooseUsername=0x7f070011;
        public static final int InsulinValue=0x7f070014;
        public static final int Login=0x7f070005;
        public static final int Password=0x7f070004;
        public static final int SignUp=0x7f070012;
        public static final int Username=0x7f070003;
        public static final int WriteMessage=0x7f070016;
        public static final int action_settings=0x7f070001;
        public static final int app_name=0x7f070000;
        public static final int dummy_button=0x7f07000d;
        public static final int dummy_content=0x7f07000e;
        public static final int glucoseValue=0x7f070015;
        public static final int hello_world=0x7f070002;
        public static final int title_activity_animation=0x7f07000c;
        public static final int title_activity_database_stream=0x7f07000b;
        public static final int title_activity_logged_in=0x7f070006;
        public static final int title_activity_message=0x7f070008;
        public static final int title_activity_new_message=0x7f070013;
        public static final int title_activity_new_profil=0x7f07000f;
        public static final int title_activity_notifications=0x7f070009;
        public static final int title_activity_profil=0x7f070007;
        public static final int title_activity_xml_parsing=0x7f07000a;
    }
    public static final class style {
        /** 
        Base application theme, dependent on API level. This theme is replaced
        by AppBaseTheme from res/values-vXX/styles.xml on newer devices.
    

            Theme customizations available in newer API levels can go in
            res/values-vXX/styles.xml, while customizations related to
            backward-compatibility can go here.
        

        Base application theme for API 11+. This theme completely replaces
        AppBaseTheme from res/values/styles.xml on API 11+ devices.
    
 API 11 theme customizations can go here. 

        Base application theme for API 14+. This theme completely replaces
        AppBaseTheme from BOTH res/values/styles.xml and
        res/values-v11/styles.xml on API 14+ devices.
    
 API 14 theme customizations can go here. 
         */
        public static final int AppBaseTheme=0x7f080000;
        /**  Application theme. 
 All customizations that are NOT specific to a particular API-level can go here. 
         */
        public static final int AppTheme=0x7f080001;
        public static final int ButtonBar=0x7f080003;
        public static final int ButtonBarButton=0x7f080004;
        public static final int FullscreenActionBarStyle=0x7f080005;
        public static final int FullscreenTheme=0x7f080002;
    }
    public static final class xml {
        public static final int items=0x7f040000;
    }
    public static final class styleable {
        /** 
         Declare custom theme attributes that allow changing which styles are
         used for button bars depending on the API level.
         ?android:attr/buttonBarStyle is new as of API 11 so this is
         necessary to support previous API levels.
    
           <p>Includes the following attributes:</p>
           <table>
           <colgroup align="left" />
           <colgroup align="left" />
           <tr><th>Attribute</th><th>Description</th></tr>
           <tr><td><code>{@link #ButtonBarContainerTheme_buttonBarButtonStyle com.example.reabetic:buttonBarButtonStyle}</code></td><td></td></tr>
           <tr><td><code>{@link #ButtonBarContainerTheme_buttonBarStyle com.example.reabetic:buttonBarStyle}</code></td><td></td></tr>
           </table>
           @see #ButtonBarContainerTheme_buttonBarButtonStyle
           @see #ButtonBarContainerTheme_buttonBarStyle
         */
        public static final int[] ButtonBarContainerTheme = {
            0x7f010000, 0x7f010001
        };
        /**
          <p>This symbol is the offset where the {@link com.example.reabetic.R.attr#buttonBarButtonStyle}
          attribute's value can be found in the {@link #ButtonBarContainerTheme} array.


          <p>Must be a reference to another resource, in the form "<code>@[+][<i>package</i>:]<i>type</i>:<i>name</i></code>"
or to a theme attribute in the form "<code>?[<i>package</i>:][<i>type</i>:]<i>name</i></code>".
          @attr name com.example.reabetic:buttonBarButtonStyle
        */
        public static final int ButtonBarContainerTheme_buttonBarButtonStyle = 1;
        /**
          <p>This symbol is the offset where the {@link com.example.reabetic.R.attr#buttonBarStyle}
          attribute's value can be found in the {@link #ButtonBarContainerTheme} array.


          <p>Must be a reference to another resource, in the form "<code>@[+][<i>package</i>:]<i>type</i>:<i>name</i></code>"
or to a theme attribute in the form "<code>?[<i>package</i>:][<i>type</i>:]<i>name</i></code>".
          @attr name com.example.reabetic:buttonBarStyle
        */
        public static final int ButtonBarContainerTheme_buttonBarStyle = 0;
    };
}
