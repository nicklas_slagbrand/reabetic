package com.example.reabetic;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

public class LoggedIn extends Activity implements OnClickListener {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE); 
		setContentView(R.layout.activity_logged_in);
		ImageView profilbtn = (ImageView) findViewById(R.id.profilbtn);
		ImageView notificationsbtn =  (ImageView) findViewById(R.id.notificationsbtn);
		ImageView messagebtn = (ImageView) findViewById(R.id.messagebtn2);
		messagebtn.setOnClickListener(this);
		profilbtn.setOnClickListener(this);
		notificationsbtn.setOnClickListener(this);
		
		//get Username from loginWindow
		String s = getIntent().getExtras().getString("sendInfo");
		View v = findViewById(R.id.theusername);
		TextView e =  (TextView) v;
		e.setText("Welcome " + s);
		
		
		//send Username to new message
		//?
		
	
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.logged_in, menu);
		return true;
	}



	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch(v.getId()){
		
		//Profilbutton
		case R.id.profilbtn:
			Intent i = new Intent (this,Profil.class);
			startActivity(i);
		break;
		
		//MessageButton
		case R.id.messagebtn2:
			Intent i2 = new Intent (this,Message.class);
			startActivity(i2);
		break;
		
		//NotificationsButton
		case R.id.notificationsbtn:
			Intent i3 = new Intent (this,Notifications.class);
			startActivity(i3);		
	 	break;
			
		
		}

		
	}

}
