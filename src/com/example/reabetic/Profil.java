package com.example.reabetic;

import java.util.ArrayList;
import java.util.HashMap;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.StrictMode;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;

public class Profil extends Activity implements OnClickListener {
	

	ListView list;
	TextView PrimaryUser;
	Button Btngetdata;
	ArrayList<HashMap<String, String>> oslist = new ArrayList<HashMap<String, String>>();
	
	//URL to get JSON Array
	private static String url = "http://192.168.0.10/~nicklasslagbrand/Reabetic/getUsers.php";
	
	//JSON Node Names 
	private static final String TAG_OS = "users";
	private static final String TAG_MSG = "username";
	
	JSONArray android = null;
	
	

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_profil);
		
		StrictMode.enableDefaults();
		 oslist = new ArrayList<HashMap<String, String>>();
		 new JSONParse().execute();
	        
	}//Oncreate ends.
	
	private class JSONParse extends AsyncTask<String, String, JSONObject> {
		 private ProgressDialog pDialog;

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			
			 PrimaryUser = (TextView)findViewById(R.id.Usertextfield);

            pDialog = new ProgressDialog(Profil.this);
            pDialog.setMessage("Getting Data ...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(true);
            pDialog.show();
		}

		@Override
		protected JSONObject doInBackground(String... params) {
			// TODO Auto-generated method stub
			JSONParser jParser = new JSONParser();

    		// Getting JSON from URL
    		JSONObject json = jParser.getJSONFromUrl(url);
    		return json;
	}

		@Override
		protected void onPostExecute(JSONObject json) {

		// TODO Auto-generated method stub
					pDialog.dismiss();
		   		 try {
		   				// Getting JSON Array from URL
		   				android = json.getJSONArray(TAG_OS);
		   				for(int i = 0; i < android.length(); i++){
		   				JSONObject c = android.getJSONObject(i);
		   				
		   				// Storing  JSON item in a Variable
		   				String ver = c.getString(TAG_MSG);

		   				
		   			
		   				
		   				
		   				// Adding value HashMap key => value
		   				

		   				HashMap<String, String> map = new HashMap<String, String>();

		   				map.put(TAG_MSG, ver);
		   				
		   				oslist.add(map);
		   				list=(ListView)findViewById(R.id.list2);
		   				
		   				
		   				
		   		        
		   				
		   				ListAdapter adapter = new SimpleAdapter(Profil.this, oslist,
		   						R.layout.ist_v2,
		   						new String[] { TAG_MSG }, new int[] {
		   								R.id.Usertextfield});

		   				list.setAdapter(adapter);
		   				list.setOnItemClickListener(new AdapterView.OnItemClickListener() {

		   		            @Override
		   		            public void onItemClick(AdapterView<?> parent, View view,
		   		                                    int position, long id) {
		   		                Toast.makeText(Profil.this, "You Clicked at "+oslist.get(+position).get("name"), Toast.LENGTH_SHORT).show();

		   		            }
		   		        });

		   				}
		   		} catch (JSONException e) {
		   			e.printStackTrace();
		   		}

		   		 
		   	 }

				}

	@Override
	public void onClick(View arg0) {
		// TODO Auto-generated method stub
		
	}



		
		
}//Class ends.
