package com.example.reabetic;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

import android.os.AsyncTask;
import android.os.Bundle;
import android.os.StrictMode;
import android.app.Activity;
import android.content.Intent;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

public class Login extends Activity implements OnClickListener{

	ImageView login;
	ImageView newuser;
	EditText username, password;
	TextView tv;
	List<NameValuePair> nameValuePairs;

	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE); 
		setContentView(R.layout.activity_login);
		StrictMode.enableDefaults();
		Clicklistenerbutton();
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.login, menu);
		return true;
	}
	
	
	
	private void Clicklistenerbutton() {
		// TODO Auto-generated method stub
		
		username = (EditText) findViewById(R.id.username);
		password = (EditText) findViewById(R.id.password);
		tv = (TextView) findViewById(R.id.Bluetoothfield);

		login =  (ImageView) findViewById(R.id.loginbtn2);
		login.setOnClickListener(this);
		
		newuser = (ImageView) findViewById(R.id.newuserbtn);
		newuser.setOnClickListener(this);

	}

	@Override
	public void onClick(View v) {
		switch(v.getId()){
		
		case R.id.loginbtn2:
		new MyLoginTask().execute(username.getText().toString().trim(), password.getText().toString().trim());
			break;
		
		case R.id.newuserbtn:
			Intent i2 = new Intent(this,NewProfil.class);
			startActivity(i2);
			break;
		
		}
	
	}	
				
	private class MyLoginTask extends AsyncTask<String, Integer, String>{

		//Connecting to Database in background
		@Override
		protected String doInBackground(String... params) {
			return login( params[0], params[1]);
		}
		
		@Override
		protected void onPostExecute(String result) {
			if( result != null ){
				tv.setText(""+result);
				Log.i("Resutlat" ,result);
;				//Checking i User was found on database				
				if(result.equalsIgnoreCase("User Found")){
					
					Intent i = new Intent(Login.this,LoggedIn.class);  
					EditText v = (EditText) findViewById(R.id.username);
					String s = v.getText().toString();
					i.putExtra("sendInfo", s);
					startActivity(i);
					finish();
				}			
			}else{
				//if User not found you cant login and a message is displayed.
				tv.setText("User Not Found");
			}
	
			
			super.onPostExecute(result);
		}
		
		private String login(String username, String password) {
			//initialize HTTP clients
			
			HttpResponse response;
			HttpClient httpclient;
			HttpPost httppost;
			
			// TODO Auto-generated method stub
			try{
				//Connecting to HTTP Client.
				httpclient=new DefaultHttpClient();
				httppost= new HttpPost("http://192.168.0.10/~nicklasslagbrand/Reabetic/check.php"); // make sure the url is correct.
				//add your data
				nameValuePairs = new ArrayList<NameValuePair>(1);
				// Always use the same variable name for posting i.e the android side variable name and php side variable name should be similar, 
				nameValuePairs.add(new BasicNameValuePair("username",username));  // $Edittext_value = $_POST['Edittext_value'];
				nameValuePairs.add(new BasicNameValuePair("password",password)); 
				httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
				//Execute HTTP Post Request
				response=httpclient.execute(httppost);
				
				ResponseHandler<String> responseHandler = new BasicResponseHandler();

				return httpclient.execute(httppost, responseHandler);
	    
			}catch(Exception e){
				e.printStackTrace();
			}
			
			return null;
	    
		}
		
	}
		
}
