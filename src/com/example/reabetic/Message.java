package com.example.reabetic;



import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;


import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.StrictMode;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.example.reabetic.JSONParser;


public class Message extends Activity implements OnClickListener{
	
	
	ListView list;
	TextView msg;
	TextView GlucoseView;
	TextView InsulinView;
	ImageView Btngetdata;
	
	ArrayList<HashMap<String, String>> oslist = new ArrayList<HashMap<String, String>>();
	
	//URL to get JSON Array
	private static String url = "http://192.168.0.10/~nicklasslagbrand/Reabetic/getJson.php";
	
	//JSON Node Names 
	private static final String TAG_OS = "ChatForum";
	private static final String TAG_MSG = "messages";
	private static final String TAG_Glucose = "glucose";
	private static final String TAG_Insulin = "insulin";
	
	JSONArray android = null;


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_message);
		StrictMode.enableDefaults();
		 oslist = new ArrayList<HashMap<String, String>>();
		 new JSONParse().execute();
	        
	        
	        Btngetdata = (ImageView)findViewById(R.id.newMessageBtn);
	        Btngetdata.setOnClickListener(new View.OnClickListener() {
				
				@Override
				public void onClick(View view) {
			         
			         ImageView Btngetdata = (ImageView) view;
			         Intent i = new Intent (Message.this,NewMessage.class);
			         startActivity(i);
			         //finish();
					
				}
			});

	}//onCreate ends.		
		
	 private class JSONParse extends AsyncTask<String, String, JSONObject> {
		 private ProgressDialog pDialog;

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			   super.onPreExecute();
	             msg = (TextView)findViewById(R.id.MessageTxtView);
				 GlucoseView = (TextView)findViewById(R.id.Usertextfield);
				 InsulinView = (TextView)findViewById(R.id.InsulinTxtView);
	            pDialog = new ProgressDialog(Message.this);
	            pDialog.setMessage("Getting Data ...");
	            pDialog.setIndeterminate(false);
	            pDialog.setCancelable(true);
	            pDialog.show();
		}

		@Override
		protected JSONObject doInBackground(String... params) {
			// TODO Auto-generated method stub
			JSONParser jParser = new JSONParser();

    		// Getting JSON from URL
    		JSONObject json = jParser.getJSONFromUrl(url);
    		return json;

		}

		@Override
		protected void onPostExecute(JSONObject json) {
			// TODO Auto-generated method stub
			pDialog.dismiss();
   		 try {
   				// Getting JSON Array from URL
   				android = json.getJSONArray(TAG_OS);
   				for(int i = 0; i < android.length(); i++){
   				JSONObject c = android.getJSONObject(i);
   				
   				// Storing  JSON item in a Variable
   				String ver = c.getString(TAG_MSG);
   				String name = c.getString(TAG_Glucose);
   				String api = c.getString(TAG_Insulin);
   				
   			
   				
   				
   				// Adding value HashMap key => value
   				

   				HashMap<String, String> map = new HashMap<String, String>();

   				map.put(TAG_MSG, ver);
   				map.put(TAG_Glucose, name);
   				map.put(TAG_Insulin, api);
   				
   				oslist.add(map);
   				list=(ListView)findViewById(R.id.list2);
   				
   				
   				
   		        
   				
   				ListAdapter adapter = new SimpleAdapter(Message.this, oslist,
   						R.layout.list_v,
   						new String[] { TAG_MSG,TAG_Glucose, TAG_Insulin }, new int[] {
   								R.id.MessageTxtView,R.id.Usertextfield, R.id.InsulinTxtView});

   				list.setAdapter(adapter);
   				list.setOnItemClickListener(new AdapterView.OnItemClickListener() {

   		            @Override
   		            public void onItemClick(AdapterView<?> parent, View view,
   		                                    int position, long id) {
   		                //Toast.makeText(Message.this, "You Clicked at "+oslist.get(+position).get("name"), Toast.LENGTH_SHORT).show();

   		            }
   		        });

   				}
   		} catch (JSONException e) {
   			e.printStackTrace();
   		}

   		 
   	 }

		}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		
	}
	
	 
	 }//messageclass ends


