package com.example.reabetic;

import java.util.ArrayList;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.StrictMode;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.ImageView;

public class NewMessage extends Activity implements OnClickListener{
	

    EditText glucoseText, insulinText, messagesText;
    ImageView sendButton;
    
    String messages;
    String glucose;
    String insulin;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE); 
		setContentView(R.layout.activity_new_message);
		StrictMode.enableDefaults();
		
		insulinText = (EditText) findViewById(R.id.insulinfield);
		glucoseText = (EditText) findViewById(R.id.glucosefield);
		messagesText = (EditText) findViewById(R.id.messagefield);
		sendButton = (ImageView) findViewById(R.id.sendMessage);
		sendButton.setOnClickListener(new View.OnClickListener(){

			@Override
			public void onClick(View v) {
				

				// TODO Auto-generated method stub
				ImageView sendButton = (ImageView) v;
				EditText e = (EditText) findViewById(R.id.messagefield);
				String s = e.getText().toString();
				Intent i = new Intent (NewMessage.this, Message.class);
				i.putExtra("", s);
				startActivity(i);
				finish();
				
				
				messages = messagesText.getText().toString();
				glucose = glucoseText.getText().toString();
				insulin = insulinText.getText().toString();
				new SummaryAsyncTask().execute((Void)null);
				
			}
			
		});//Onclick Ends.
		
		
		
	}//onCreate ends.

	class SummaryAsyncTask extends AsyncTask<Void, Void, Boolean>{
		
		private void postData( String messages, String glucose, String insulin){
			HttpClient httpClient = new DefaultHttpClient();
			HttpPost httpPost = new HttpPost ("http://192.168.0.10/~nicklasslagbrand/Reabetic/sendMessage.php");
			try{
				ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(4);
				nameValuePairs.add(new BasicNameValuePair("messages", messages));
				nameValuePairs.add(new BasicNameValuePair("glucose", glucose));
				nameValuePairs.add(new BasicNameValuePair("insulin", insulin));
                httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
                HttpResponse response = httpClient.execute(httpPost);
                Log.e("tag", "msg");
			}
			catch(Exception e){
				Log.e("log_tag", "Error:  "+e.toString());	
			}
		}

		@Override
		protected Boolean doInBackground(Void... params) {
			// TODO Auto-generated method stub
			postData(messages, glucose, insulin);
			return null;
		}
	
	}

	@Override
	public void onClick(View arg0) {
		// TODO Auto-generated method stub
		
	}

}//Class ends.
