package com.example.reabetic;

import java.util.ArrayList;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;


import android.os.AsyncTask;
import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;

public class NewProfil extends Activity implements OnClickListener {
	
	
	String password;
    String username;

    EditText passwordText, usernameText;
    ImageView button;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE); 
		setContentView(R.layout.activity_new_profil);

		
        usernameText = (EditText) findViewById(R.id.chooseUsername);
        passwordText = (EditText) findViewById(R.id.choosePassword);
        button = (ImageView) findViewById(R.id.confirmbtn);
        button.setOnClickListener(new View.OnClickListener() {
	@Override
	public void onClick(View arg0) {
		// TODO Auto-generated method stub
		ImageView button =  (ImageView) arg0;
		EditText e = (EditText) findViewById(R.id.chooseUsername);
		String s = e.getText().toString();
		Intent i = new Intent(NewProfil.this,LoggedIn.class);
		i.putExtra("sendInfo", s);
		startActivity(i);
		finish();
        username = usernameText.getText().toString();
		password = passwordText.getText().toString();

        new SummaryAsyncTask().execute((Void) null);
		
	}
        });//ClickListener ends.

}
	class SummaryAsyncTask extends AsyncTask<Void, Void, Boolean> {

        private void postData(String username, String password) {

            HttpClient httpclient = new DefaultHttpClient();
            HttpPost httppost = new HttpPost("http://192.168.0.10/~nicklasslagbrand/Reabetic/Loadup.php");

            try {
                ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(4);
                nameValuePairs.add(new BasicNameValuePair("username", username));
                nameValuePairs.add(new BasicNameValuePair("password", password));
                httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
                HttpResponse response = httpclient.execute(httppost);
            }
            catch(Exception e)
            {
                Log.e("log_tag", "Error:  "+e.toString());
            }
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            postData(username, password);
            return null;
        }
    }
	//nextactivity on click.
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub

		
	}	
	
}
